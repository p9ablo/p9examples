#python3.6
import secrets

n_bits = 32

secret_key = secrets.token_urlsafe(n_bits)

print(len(secret_key))
print(secret_key)