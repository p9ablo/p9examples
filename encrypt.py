from Crypto.Cipher import AES
import base64
import argparse


def encrypt(message, secret_key):
    msg_text = message.rjust(32)
    cipher = AES.new(secret_key,AES.MODE_ECB) # never use ECB in strong systems obviously
    encoded = base64.b64encode(cipher.encrypt(msg_text))

    return encoded

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='It receive key_secret and plain message then return encrypted message')
    parser.add_argument('-m', action="store", dest="message", help="original message to encrypt")
    parser.add_argument('-s', action="store", dest="secret_key", help="secret key to decrypt message")

    results = parser.parse_args()
    print(encrypt(results.message, results.secret_key))