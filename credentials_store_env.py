import os
"""
requirements python-dotenv
"""
from dotenv import load_dotenv
project_folder = os.path.expanduser('~/examples')  # adjust as appropriate
load_dotenv(os.path.join(project_folder, '.env'))
print(os.environ.get("SECRET_KEY", "None"))