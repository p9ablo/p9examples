from Crypto.Cipher import AES
import base64
import argparse

def decrypt(encrypted_message, secret_key):
    cipher = AES.new(secret_key,AES.MODE_ECB) # never use ECB in strong systems obviously
    decoded = cipher.decrypt(base64.b64decode(encrypted_message))

    return decoded.strip()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='It receive key_secret and encrypted message then retun original decrypted message')
    parser.add_argument('-m', '--message', action="store", type=str, dest="encrypted_message", help="encrypted message")
    parser.add_argument('-s', '--secret', action="store", dest="secret_key", help="secret key to encrypt message")

    results = parser.parse_args()
    print(decrypt(results.encrypted_message, results.secret_key))