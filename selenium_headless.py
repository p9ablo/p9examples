from pyvirtualdisplay import Display
from selenium import webdriver
"""
run on bash console next: (or python)
xvfb-run -a python3.6 /home/myusername/myfolder/myscript.py
"""
with Display():
    profile=webdriver.FirefoxProfile()
    profile.set_preference('network.proxy.type', 1)
    profile.set_preference('network.proxy.socks', '127.0.0.1')
    profile.set_preference('network.proxy.socks_port', 9150)

    # we can now start Firefox and it will run inside the virtual display
    browser = webdriver.Firefox(profile)

    # put the rest of our selenium code in a try/finally
    # to make sure we always clean up at the end
    try:
        browser.get('http://www.cualesmiip.com')
        browser.save_screenshot("screenshot.png")
        print(browser.title) #this should print "Google"

    finally:
        browser.quit()